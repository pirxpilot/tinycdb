tinycdb (0.78) unstable; urgency=low

  * new release (0.78), a few minor fixes:
   - fix handling of files >4Gb
   - fix handling of files >2Gb on 32bit platforms (compile with _FILE_OFFSET_BITS=64)
   - fix file size limit test
   - fix compiler warnings (missing #include)
  * make it multiarch-aware (install libs to /usr/lib/$arch)
  * use dpkg-buildflags for CFLAGS/LDFLAGS
  * update Standards-Version to 3.9.3
  * debhelper 7
  * simplify debian/rules quite a bit

 -- Michael Tokarev <mjt@tls.msk.ru>  Fri, 11 May 2012 16:33:34 +0400

tinycdb (0.77) unstable; urgency=low

  * manpage typo fixes.

  * in win32, close file mapping handle right after MapViewOfFile(),
    instead of doing it in cdb_close().  This eliminates handle leak
    on win32 platform.  And this is how mmap() works on unix, too.
    Thanks to David Boyce for this.

  * also for win32 platform, cdb.c (the utility):
    - open cdb files in binary mode
    - add windowsisms for include files
    Also thanks to David Boyce for the fixes.

  * fixed a bug in _cdb_make_fullwrite() - wrong logic in EINTR
    handling.
    Thanks to Florian Weimer.
    Closes: #511629.

  * install /usr/lib/pkgconfig/libcdb.pc from debian/rules.
    Closes: #446751, #446752.

  * lintian warnings:
    o moved $DH_COMPAT to deban/compat (=4)
    o set Standards-Version: 3.8.0 (no changes needed)
    o fixed "package package" in libcdb1 description (Closes: #442571)
    o s/${Source-Version}/${binary:Version}/

  * released 0.77

 -- Michael Tokarev <mjt@corpit.ru>  Sat, 31 Jan 2009 20:12:02 +0300

tinycdb (0.76) unstable; urgency=low

  * 0.76 release.
    Closes: #342849, #316253, #360129, #383417.
    Also closes: #299026, #344572.

  * manpage spelling fixes, from Claus Assmann <ca+tinycdb (at) esmtp.org>.

  * little mods to allow compiling tinycdb by C++ compiler,
    from Olly Betts <olly (at) survex.com>.

  * use program_invocation_short_name on GLIBC, (modified) patch
    from Dmitry V. Levin  <ldv (at) altlinux.org>

  * manpage fix (cdb_findnext() prototype),
    from Dmitry V. Levin <ldv (at) altlinux.org>

  * (somewhat silly) GCC-4.x "signedness" warnings fix, modified patch
    from Dmitry V. Levin  <ldv (at) altlinux.org>

  * more signed vs unsigned char* fixes in various places

  * Makefile: always build libnss_cdb.so with libcdb_pic.a, no nss-shared
    target: to avoid extra dependency from /usr/lib/.

  * Makefile: use map files for lib*.so, with explicit list of exported
    symbols.  This, in particular, avoids exporting of libcdb symbols by
    libnss_cdb.so.

  * mark all internal routines as internal_function (defined as
    __attribute__((visibility("hidden"))) for GCC)

  * Makefile: add tests-shared, to use cdb-shared for testing

  * Makefile: allow to specify which binary (shared vs static) to install
    in install target, by using INSTALLPROG variable

  * Makefile: pass -DNSSCDB_DIR=... to compiler when building .lo files,
    to allow setting of system config dir (/etc by default) on command line.
    For nss_cdb module.

  * Makefile: use $(CP) instead of cp, to be able to specify `cp' options
    (CP="cp -p")

  * Makefile: ship debian/ files in `dist' target as well, if not only
    for debian/changelog.

  * Makefile: bumped version to 0.76

  * Use unlink(tmpname) + open(O_CREAT|O_EXCL) instead of
    open(O_CREAT|O_TRUNC) when creating the new CDB file.
    And use O_NOFOLLOW if defined.
    This also works around some (probably) possible symlink attacks.

  * Add -p perms option for cdb utility, to specify mode (permission bits)
    for newly created .cdb file (default is 0666&umask(), -p forces the given
    mode).

  * allow tmpname (cdb -t) to be `-', to mean no temp file gets created.
    Also check if tmpname is the same as database name and don't try to
    rename() if it is.

  * rewrite nss_cdb-Makefile a bit: simplify it, and use more sane
    permission scheme for /etc/shadow

  * bumped Debian Standards-Version to 3.7.2 (no changes necessary).

  * fixed a typo in cdb_seek.c, -- it segfaulted if passed NULL dlenp
    pointer.  Thanks Daiki for the patch.  Closes: #383417

  * use MAP_FAILED to test mmap() return value, instead of hardcoded -1.

  * several minor fixes for debian/* files, thanks to Bernhard R. Link.

  * removed libnss_cdb Debian package, for now

  * libcdb-dev replaces tinycdb<0.76

 -- Michael Tokarev <mjt@corpit.ru>  Sat,  9 Sep 2006 13:55:10 +0400

tinycdb (0.75) unstable; urgency=low

  * 0.75 release

  * debian packaging is now back to me.  Thank you Christian for doing
    packaging work for me, it is greatly apprecated.

  * build 4 packages out of the source base:
    - shared library libcdb1
    - development files libcdb-dev
    - utility tinycdb
    - nss module nss-cdb
    Closes: Debian #360129, #316253.

  * rewrote cdb_make_put(CDB_PUT_REPLACE) to a) actually replace *all*
    duplicates, not just the first one, and b) perform real replace,
    by moving tail of .cdb file.  Also, always add new record to the
    end of the file, not to the place where a duplicate was found.

  * add cdb_make_put(CDB_PUT_REPLACE0) to zerofill all duplicates,
    which is faster than CDB_PUT_REPLACE but less accurate as it
    leaves gaps in the file.

  * ship libcdb_pic.a in the -dev package.

 -- Michael Tokarev <mjt@corpit.ru>  Tue, 23 Aug 2005 20:06:01 +0400

tinycdb (0.74-1) unstable; urgency=low

  * New upstream release.

 -- Christian Kurz <shorty@debian.org>  Wed, 28 Jul 2004 20:12:02 +0200

2006-06-29  Michael Tokarev  <mjt@corpit.ru>

	* see debian/changelog file for further changes.

2005-04-18  Michael Tokarev  <mjt@corpit.ru>

	* move cdb_make_find.c content into cdb_make_put.c

	* introduce CDB_PUT_REPLACE0 - zerofill old duplicates

	* allow usage of cdb.h in C++

2005-04-11  Michael Tokarev  <mjt@corpit.ru>

	* do not autogenerate files (cdb.h.in, cdb.3.in etc), but
	  use real files instead (only substituted VERSION and NAME)

	* finally fixed the `!fputs()' condition to be `fputs() < 0'
	  as it should be in cdb.c (misbehaves on *bsd)

	* kill cdbi_t usage in cdb_int.h

	* export _cdb_make_fullwrite() (was ewrite()) and _cdb_make_flush()
	  and use them in cdb_make.c as appropriate

	* almost completely rewrite _cdb_make_find() and friends:
	  - _cdb_make_find() now accepts new parameter, remove (bool), which,
	    if true, indicates all found records should be deleted from the
	    database.
	  - Call _cdb_make_find() with remove=0 from cdb_make_exists()
	  - Call _cdb_make_find() with appropriate arguments from
	    cdb_make_put(), and simplify the latter greatly (was too clumsy
	    anyway)

	* rename `flags' parameter in cdb_make_put() to be `mode' which is
	  more appropriate

	* change #if conditional in nss_cdb.c
	  from __GLIBC__ to __GNU_LIBRARY__

2003-11-04  Michael Tokarev  <mjt@corpit.ru>

	* added cdb_get() routine: tinycdb officially uses mmap.

	* added cdb_{get,read}{data,key}() macros to read and get
	  current data and key.

	* fixed bug in cdb_seek() - incorrect wrap, sometimes
	  cdb_seek()+cdb_bread() may return EIO instead of finding
	  correct record.

	* added some tweaks to Makefile to build position-independent
	  libcdb_pic.a and shared libcdb.so libraries.  Note that
	  using libcdb as shared library is probably not a good idea,
	  due to tiny size of the library.

	* added initial nss_cdb module.  Still not well-tested.
	  Probably will not build on non-GNU system.

	* adjusted tests.{ok,sh} for latest cdb utility modifications
	  (-a mode in query by default)

	* Victor Porton (porton at ex-code.com) provided a patch
	  to allow tinycdb to be built on win32 platform (cdb_init.c).
	  Completely untested.

2003-08-13  Michael Tokarev  <mjt@corpit.ru>

	* s/cdbi_t/unsigned/g.  No need to keep this type.

	* changed usage of cdb_findnext(): one need to pass
	  pointer to cdb structure to cdb_findnext() now,
	  and should use cdb_datapos(struct cdb_find *)
	  instead of cdb_datapos(struct cdb *)

	* added cdb_seqinit() and cdb_seqnext() routines for sequential
	  record enumeration

	* addded cdb_dend to the cdb structure: end of data
	  position.  Use that in cdb_seq*().

	* more strict checking: ensure data is within data section,
	  and hash tables are within hash section of a file.

	* cdb_make.c (cdb_make_start): zerofill cdb_make structure
	  to shut valgrind up (writing uninitialized data to file)

	* cdb.c (cmode): always open file in RDWR mode to allow
	  duplicate key detection

2002-12-08  Michael Tokarev  <mjt+cdb@corpit.ru>

	* version 0.73
	* de-Debianization.  Oh well... ;)
	* no code changes, just like in 0.72

2002-10-13  Michael Tokarev  <mjt+cdb@corpit.ru>

	* version 0.72

	* cleaned up debian packaging and made it actually work

	* no code changes

2002-07-22  Michael Tokarev <mjt+cdb@corpit.ru>

	* version 0.71

	* rearranged object files to not depend on ranlib on
	  systems that requires it (i.e. OpenBSD)

	* use ranlib but mark it's possible error as non-fatal

2001-12-10  Michael Tokarev <mjt+cdb@corpit.ru>

	* version 0.7a

	* converted to CVS, added two missing #include <stdlib.h> for
	  malloc declaration and spec target to the Makefile

2001-10-14  Michael Tokarev <mjt+cdb@corpit.ru>

	* version 0.7

	* added cdb_seek() and cdb_bread() routines as found
	  in freecdb/cdb-0.64

2001-07-26  Michael Tokarev <mjt+cdb@corpit.ru>

	* version 0.6

	* added another option, CDB_PUT_WARN, to cdb_make_put's flags
	  (to allow adding unconditionally but still warn about dups),
	  now cdb_make_put seems to be logically complete.

	* added and documented -r and -u options for cdb(1) command,
	  and made them consistent with -w and -e also.

	* reorganized cdb(1) manpage and added changes made to cdb
	  command.

	* added version references to manpages (and make them autogenerated
	  to simplify maintenance).

	* added cdb(5) manpage describing CDB file format.

2001-07-25  Michael Tokarev <mjt+cdb@corpit.ru>

	* version 0.5

	* added missing #include <sys/types.h> in cdb_init.c, thanks to
	  ppetru@ppetru.net (Petru Paler)

	* removed usage of pread() in cdb_make_find() and friends,
	  suggested by Liviu Daia <Liviu.Daia@imar.ro>

	* autogenerate tinycdb.spec file from template and debian/changelog

	* autogenerate cdb.h from cdb.h.in (substituting version)

2001-06-29  Michael Tokarev <mjt+cdb@corpit.ru>

	* version 0.4

	* added cdb_make_put() routine to conditionnaly add a record

	* split cdb library to more files (finer granularity)

	* added cdb_findinit() and cdb_findnext() routines

	* renamed cdbtool to cdb

	* simplified cdb utility (dropped various format spec, changed
	  options parsing) and a manpage

	* added small note and copyright to every file in package

	* added some testsuite (make test)

2001-05-27  Michael Tokarev <mjt+cdb@corpit.ru>

	* version 0.3

	* Initial Release.
